/*
const - const means something 'cannot' change. Use it liberally
*/


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define ROWS 5
#define COLS 5
int main()
{
         char ** board = (char **)malloc(ROWS * sizeof(char)); 
         board[0] = (char *)malloc(COLS * sizeof(char));
         board[0][0] = 'h';
         board[0][1] = 'i';
         printf("%c", board[0][0]);
         printf("%c", board[0][1]);
         free(board[0]);
         free(board);
}


int       *      //mutable_pointer_to_mutable_int;
int const *      //mutable_pointer_to_constant_int;
int       *//const constant_pointer_to_mutable_int;
int const *//const constant_pointer_to_constant_int;
                 